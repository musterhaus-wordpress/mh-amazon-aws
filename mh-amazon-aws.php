<?php

/*
Plugin Name: Musterhaus - Amazon AWS
Plugin URI:
Description: Amazon SDK für Wordpress.
Version: 1.0
Author: Sebastian Hübner <huebner@musterhaus.net>
Author URI:
License: GPL2
Text Domain: mh-amazon-aws
*/

/**
 * Create Upload Object and initialize it
 */
$s3Upload = new \Musterhaus\AmazonAwsWP\S3\MediaUpload();
$s3Upload->init();