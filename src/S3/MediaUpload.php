<?php

namespace Musterhaus\AmazonAwsWP\S3;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Credentials\Credentials as AwsCredentials;

/**
 * Class S3Upload
 * @package Musterhaus\AmazonAwsWP
 */
class MediaUpload
{

    protected $attachment_id;
    protected $guid;

    /**
     */
    public function init()
    {
        add_filter('pre_option_upload_url_path', [&$this, "define_upload_url_path"], 100);

        // Add Attachment
        add_action('add_attachment', [&$this, 'upload_image_to_S3'], 50);
        add_action('edit_attachment', [&$this, 'update_upload_image_to_S3'], 50);
        add_filter('wp_generate_attachment_metadata', [&$this, 'upload_thumbnails_to_s3'], 50);

        // Delete Attachment
        add_action('delete_attachment', [&$this, 'delete_attachment_from_S3'], 50);

        if (is_admin()) {
            add_action('admin_menu', [&$this, 'add_admin_menu']);
            add_action('admin_init', array($this, 'register_options_init'));
        }
    }

    /**
     * @return S3Client
     */
    public function get_S3_client()
    {
        $options = get_option('mh_aws_s3_options_settings');
        $credentials = new AwsCredentials(getenv('AWS_KEY_ID'), getenv('AWS_SECRET_ACCESS_KEY'));

        $s3 = new S3Client([
            'version' => 'latest',
            'region' => $options['region'],
            'credentials' => $credentials,
            'http' => [
                'verify' => false,
            ],
        ]);

        return $s3;
    }

    /**
     * Lade eine Datei nach übergebenen Dateipfad auf den Amazon S3 Server hoch.
     *
     * @param $filepath
     * @return bool
     */
    public function upload_to_S3($filepath)
    {
        if (file_exists($filepath)) {
            $options = get_option('mh_aws_s3_options_settings');

            try {
                $s3 = $this->get_S3_client();
                $key = $options['upload_folder'] . '/' . pathinfo($filepath, PATHINFO_BASENAME);

                $s3->putObject([
                    "Bucket" => $options['bucket'],
                    "Key" => $key,
                    "Body" => fopen($filepath, 'r'),
                    "ACL" => "public-read",
                    "CacheControl" => "max-age=2592000",
                    "Tagging" => "category=wordpress"
                ]);

                $this->guid = $s3->getObjectUrl($options['bucket'], $key);

                return true;
            } catch (S3Exception $e) {
                error_log("Upload to S3 failed: " . $e->getMessage());
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function define_upload_url_path()
    {
        $options = get_option('mh_aws_s3_options_settings');

        if (!$options['active']) {
            remove_filter('pre_option_upload_url_path', [&$this, "define_upload_url_path"]);
        }

        if (isset($options['upload_folder']) && isset($options['cloudfront_url'])) {
            return 'https://' . $options['cloudfront_url'] . '/' . $options['upload_folder'];
        }
    }

    /**
     * @param $attachment_id
     */
    public function upload_image_to_S3($attachment_id)
    {
        $this->attachment_id = $attachment_id;
        $options = get_option('mh_aws_s3_options_settings');

        if (!$options['active']) {
            remove_action('wp_generate_attachment_metadata', [&$this, 'upload_thumbnails_to_s3']);
            remove_action('add_attachment', array(&$this, 'upload_image_to_S3'));
            return;
        }

        try {
            $s3 = $this->get_S3_client();
            $attachment_path = get_attached_file($attachment_id);
            $key = $options['upload_folder'] . '/' . pathinfo($attachment_path, PATHINFO_BASENAME);

            $s3->putObject([
                "Bucket" => $options['bucket'],
                "Key" => $key,
                "Body" => fopen($attachment_path, 'r'),
                "ACL" => "public-read",
                "CacheControl" => "max-age=2592000",
                "Tagging" => "category=wordpress"
            ]);

            $this->guid = $s3->getObjectUrl($options['bucket'], $key);
        } catch (S3Exception $e) {
            error_log("Upload to S3 failed: " . $e->getMessage());
        }
    }

    /**
     * @param $attachment_id
     */
    public function update_upload_image_to_S3($attachment_id)
    {
        $this->attachment_id = $attachment_id;
        $options = get_option('mh_aws_s3_options_settings');

        if (!$options['active']) {
            remove_action('edit_attachment', array(&$this, 'upload_image_to_S3'));
            return;
        }

        try {
            $s3 = $this->get_S3_client();
            $attachment_path = get_attached_file($attachment_id);
            $key = $options['upload_folder'] . '/' . pathinfo($attachment_path, PATHINFO_BASENAME);

            $s3->putObject([
                "Bucket" => $options['bucket'],
                "Key" => $key,
                "Body" => fopen($attachment_path, 'r'),
                "ACL" => "public-read",
                "CacheControl" => "max-age=725760",
                "Tagging" => "category=wordpress"
            ]);

            $this->guid = $s3->getObjectUrl($options['bucket'], $key);

            if (wp_attachment_is_image($attachment_id)) {
                $attachment_data = wp_get_attachment_metadata($attachment_id);
                $this->upload_thumbnails_to_s3($attachment_data);
            }
        } catch (S3Exception $e) {
            error_log($e->getMessage());
        }
    }

    /**
     * @param $attachment_data
     * @return mixed
     */
    public function upload_thumbnails_to_s3($attachment_data)
    {
        $options = get_option('mh_aws_s3_options_settings');

        if ($options['active'] == 1) {

            $attachment_id = $this->attachment_id;
            if (empty($attachment_id)) {
                global $wpdb;
                $post = $wpdb->get_row($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_value = %s LIMIT 1", $attachment_data['file']));
                $attachment_id = $post->post_id;
            }

            if (wp_attachment_is_image($attachment_id)) {
                $s3 = $this->get_S3_client();

                $file_basename = basename($attachment_data['file']);
                $file_name = pathinfo($file_basename, PATHINFO_FILENAME);
                $file_extension = pathinfo($file_basename, PATHINFO_EXTENSION);

                $upload_dir = wp_get_upload_dir();
                $upload_path = $upload_dir["path"];

                $sizes = $this->get_image_sizes();
                if (isset($attachment_data['sizes'])) {
                    $sizes = $attachment_data['sizes'];
                }

                foreach ($sizes as $key => $size) {
                    $thumbnail_path = $upload_path . '/' . $file_name . '-' . $size['width'] . 'x' . $size['height'] . '.' . $file_extension;
                    $key = $options['upload_folder'] . '/' . pathinfo($thumbnail_path, PATHINFO_BASENAME);

                    if (file_exists($thumbnail_path) !== false) {
                        try {
                            $s3->putObject([
                                "Bucket" => $options['bucket'],
                                "Key" => $key,
                                "Body" => fopen($thumbnail_path, 'r'),
                                "ACL" => "public-read",
                                "CacheControl" => "max-age=2592000",
                                "Tagging" => "category=resized&dimensions=" . $size['width'] . 'x' . $size['height'],
                                "StorageClass" => "REDUCED_REDUNDANCY"
                            ]);
                        } catch (S3Exception $e) {
                            error_log($e->getMessage());
                        }
                    }
                }
            }
        }

        return $attachment_data;
    }

    /**
     * Get size information for all currently-registered image sizes.
     *
     * @global $_wp_additional_image_sizes
     * @uses   get_intermediate_image_sizes()
     * @return array $sizes Data for all currently-registered image sizes.
     */
    public function get_image_sizes()
    {
        global $_wp_additional_image_sizes;
        $sizes = array();

        foreach (get_intermediate_image_sizes() as $_size) {
            if (in_array($_size, array('thumbnail', 'medium', 'medium_large', 'large'))) {
                $sizes[$_size]['width'] = get_option("{$_size}_size_w");
                $sizes[$_size]['height'] = get_option("{$_size}_size_h");
                $sizes[$_size]['crop'] = (bool)get_option("{$_size}_crop");
            } elseif (isset($_wp_additional_image_sizes[$_size])) {
                $sizes[$_size] = array(
                    'width' => $_wp_additional_image_sizes[$_size]['width'],
                    'height' => $_wp_additional_image_sizes[$_size]['height'],
                    'crop' => $_wp_additional_image_sizes[$_size]['crop'],
                );
            }
        }

        return $sizes;
    }

    /**
     * @param $attachment_id
     */
    public function delete_attachment_from_S3($attachment_id)
    {
        $options = get_option('mh_aws_s3_options_settings');

        if (!$options['active']) {
            remove_action('delete_attachment', array(&$this, 'delete_attachment_from_S3'));
            return;
        }

        try {
            $s3 = $this->get_S3_client();
            $attachment_path = get_attached_file($attachment_id);
            $key = $options['upload_folder'] . '/' . pathinfo($attachment_path, PATHINFO_BASENAME);

            $s3->deleteObject([
                "Bucket" => $options['bucket'],
                "Key" => $key,
            ]);

            if (wp_attachment_is_image($attachment_id)) {
                $attachment_data = wp_get_attachment_metadata($attachment_id);
                $file_basename = basename($attachment_path);
                $file_name = pathinfo($file_basename, PATHINFO_FILENAME);
                $file_extension = pathinfo($file_basename, PATHINFO_EXTENSION);

                $upload_dir = wp_get_upload_dir();
                $upload_path = $upload_dir["path"];

                $sizes = $this->get_image_sizes();
                if (isset($attachment_data['sizes'])) {
                    $sizes = $attachment_data['sizes'];
                }

                foreach ($sizes as $key => $size) {
                    $thumbnail_path = $upload_path . '/' . $file_name . '-' . $size['width'] . 'x' . $size['height'] . '.' . $file_extension;
                    $key = $options['upload_folder'] . '/' . pathinfo($thumbnail_path, PATHINFO_BASENAME);

                    if (file_exists($thumbnail_path) !== false) {
                        try {
                            $s3->deleteObject([
                                "Bucket" => $options['bucket'],
                                "Key" => $key,
                            ]);
                        } catch (S3Exception $e) {
                            error_log($e->getMessage());
                        }
                    }
                }
            }
        } catch (S3Exception $e) {
            error_log($e->getMessage());
        }
    }

    /**
     *
     */
    public function add_admin_menu()
    {
        add_options_page('Musterhaus.net - Amazon S3', 'Musterhaus.net - Amazon S3', 'manage_options', 'mh-amazon-s3', [$this, 'settings_page']);
    }

    /**
     *
     */
    public function register_options_init()
    {
        register_setting('mh_aws_s3_options', 'mh_aws_s3_options_settings');

        add_settings_section(
            'mh_aws_s3_options_section',
            __('General settings'),
            array($this, 'settings_section_callback'),
            'mh_aws_s3_options'
        );

        add_settings_field(
            'upload_folder',
            __('Local Upload Folder'),
            array($this, 'upload_folder_render'),
            'mh_aws_s3_options',
            'mh_aws_s3_options_section'
        );

        add_settings_field(
            'active',
            __('Activate Upload to S3?'),
            array($this, 'active_render'),
            'mh_aws_s3_options',
            'mh_aws_s3_options_section'
        );

        add_settings_field(
            'delete_from_filesystem',
            __('DELETE File FROM Filesystem after upload TO S3?'),
            array($this, 'delete_from_filesystem_render'),
            'mh_aws_s3_options',
            'mh_aws_s3_options_section'
        );

        add_settings_field(
            'bucket',
            __('Bucket Name on S3'),
            array($this, 'bucket_render'),
            'mh_aws_s3_options',
            'mh_aws_s3_options_section'
        );

        add_settings_field(
            'region',
            __('S3 Region'),
            array($this, 'region_render'),
            'mh_aws_s3_options',
            'mh_aws_s3_options_section'
        );

        add_settings_field(
            'cloudfront_url',
            __('Cloudfront URL for S3 Bucket'),
            array($this, 'cloudfront_url_render'),
            'mh_aws_s3_options',
            'mh_aws_s3_options_section'
        );

        // Set default options
        $options = get_option('mh_aws_s3_options_settings');
        if (false === $options) {
            // Get defaults
            $defaults = [
                'active' => false,
                'delete_from_filesystem' => false,
                'bucket' => null,
                'region' => 'eu-west-1',
                'cloudfront_url' => null,
            ];
            update_option('mh_aws_s3_options_settings', $defaults);
        }
    }

    /**
     *
     */
    public function settings_section_callback()
    {
    }

    /**
     *
     */
    public function upload_folder_render()
    {
        $options = get_option('mh_aws_s3_options_settings');
        $upload_folder = !empty($options['upload_folder']) ? $options['upload_folder'] : '';
        ?>
        <input name="mh_aws_s3_options_settings[upload_folder]" value="<?php echo $upload_folder ?>">
        <?php
    }

    /**
     *
     */
    public function active_render()
    {
        $options = get_option('mh_aws_s3_options_settings');
        ?>
        <select name="mh_aws_s3_options_settings[active]">
            <option value="0" <?php selected($options['active'], 0) ?>>Nein</option>
            <option value="1" <?php selected($options['active'], 1) ?>>Ja</option>
        </select>
        <?php
    }

    /**
     *
     */
    public function delete_from_filesystem_render()
    {
        $options = get_option('mh_aws_s3_options_settings');
        ?>
        <select name="mh_aws_s3_options_settings[delete_from_filesystem]">
            <option value="0" <?php selected($options['delete_from_filesystem'], 0) ?>>Nein</option>
            <option value="1" <?php selected($options['delete_from_filesystem'], 1) ?>>Ja</option>
        </select>
        <?php
    }

    /**
     *
     */
    public function bucket_render()
    {
        $options = get_option('mh_aws_s3_options_settings');
        ?>
        <input name="mh_aws_s3_options_settings[bucket]" value="<?php echo $options['bucket'] ?>">
        <?php
    }

    /**
     *
     */
    public function region_render()
    {
        $options = get_option('mh_aws_s3_options_settings');
        ?>
        <input name="mh_aws_s3_options_settings[region]" value="<?php echo $options['region'] ?>">
        <?php
    }

    /**
     *
     */
    public function cloudfront_url_render()
    {
        $options = get_option('mh_aws_s3_options_settings');
        ?>
        <input class="widefat" name="mh_aws_s3_options_settings[cloudfront_url]"
               value="<?php echo $options['cloudfront_url'] ?>">
        <?php
    }

    /**
     *
     */
    public function settings_page()
    {
        ?>
        <div class="wrap">
            <h1>Musterhaus.net - Amazon S3 - Einstellungen</h1>

            <div class="">
                <form action='options.php' method='post'>
                    <?php
                    settings_fields('mh_aws_s3_options');
                    do_settings_sections('mh_aws_s3_options');
                    submit_button();
                    ?>
                </form>
            </div>
        </div>
        <?php
    }

}